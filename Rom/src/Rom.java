import java.util.Scanner;

public class Rom {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String zeichen;
		int zahl = 0;
		

		System.out.println("Bitte geben Sie ein r�misches Zahlenzeichen ein./n M�glich ist: I, V, X, L, C, D, M.");
		zeichen = sc.next();
		
		switch (zeichen) {
		case "I":
		case "i":
			zahl = 1; 
			break;
		case "V":
		case "v":
			zahl = 5;
			break;
		case "X":
		case "x":
			zahl = 10;
			break;
		case "L":
		case "l":
			zahl = 50;
			break;
		case "C":
		case "c":
			zahl = 100;
			break;
		case "D":
		case "d":
			zahl = 500;
			break;
		case "M":
		case "m":
			zahl = 1000;
			break;
		default:
			System.out.println("Es wurde eine falsches Zeichen eingegeben.");
		}
		
		
		System.out.println("Die Dezimalzahl zur r�mischen Zahl lautet: " + zahl);
		
	}
}
