package ArrayListÜbung;

import java.text.MessageFormat;
import java.util.*;

public class Aufgabe {

	//ArrayList Aufgabe
	public static void main(String[] args) {

		ArrayList<Integer> myList = new ArrayList<Integer>();
		Random r = new Random();
		Scanner sc = new Scanner(System.in);

		//1
		
		for (int i = 0; i < 21; i++) {
			int j = r.nextInt(9) + 1;
			myList.add(j);
		}

		//2
		System.out.println("Ausgabe: Liste mit 20 Zufallszahlen");
		for (int i = 0; i < myList.size(); i++) {
			System.out.println(MessageFormat.format("myList [{0}]: {1}", i <= 9 ? " " + i : i, myList.get(i)));
		}

		//3 & 4
		System.out.println("\nEine Zahl zwischen 1 und 9:");
		int j = sc.nextInt();
		int zaehler = 0;

		for (Integer k : myList) {
			if (j == k) {
				zaehler++;
			}
		}
		System.out.println("Die Zahl "+j+" kommt "+zaehler+" in der Liste vor.");
		

		//5
		for (int i = 0; i < myList.size(); i++) {
			if (myList.get(i) == j) {
				System.out.println("myList ["+i+"] : "+myList.get(i));
			}
		}
		
		//6 & 7
		System.out.println("\nListe nach Löscheung "+j);
		for (int i = 0; i < myList.size(); i++) {
			if(myList.get(i) == j) {
				myList.remove(i);
			}
			
		}
		for (int i = 0; i < myList.size(); i++) {
			System.out.println(MessageFormat.format("myList [{0}]: {1}", i <= 9 ? " " + i : i, myList.get(i)));
		}
		
		//8 & 9
		System.out.println("\nListe nach Einfügen von 0 hinter jeder 5");
		for(int i = 0; i < myList.size(); i++) {
			if(myList.get(i) == 5) {
				myList.add(i + 1, 0);
				i++;
			}
		}
		
		for (int i = 0; i < myList.size(); i++) {
			System.out.println(MessageFormat.format("myList [{0}]: {1}", i <= 9 ? " " + i : i, myList.get(i)));
		}
	}
}
