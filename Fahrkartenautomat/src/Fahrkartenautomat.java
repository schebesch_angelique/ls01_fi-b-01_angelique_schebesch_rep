﻿import java.util.*;

class Fahrkartenautomat {
    public static void main(String[] args)
    {
    	while (true) {
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    		fahrkartenAusgeben();
    		try {
    			Thread.sleep(2000);
    		} catch (InterruptedException e) {
				e.printStackTrace();
    		}
    	}
    }
    
	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
	    String[] fahrkartenbezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
		double[] fahrkartenpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		int kartenTyp = 0;
		double zuZahlenderBetrag;
		
		System.out.println("Fahrkartenbestellvorgang\n"
					+ "=========================\n");
		
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin ABC aus: \n");
		for (int i = 0; i < fahrkartenbezeichnung.length; i++) {
			System.out.printf("(" + (i + 1) + ") " + fahrkartenbezeichnung[i] + " [%.2f EUR] \n", fahrkartenpreise[i]);
		}
		
		System.out.print("\nIhre Wahl:");
		kartenTyp = tastatur.nextInt();
			
		while (kartenTyp < 1 || kartenTyp > 10) {
			System.out.println(">>falsche Eingabe<<");
			System.out.print("\nIhre Wahl:");
			kartenTyp = tastatur.nextInt();
	    }   
		
		zuZahlenderBetrag = fahrkartenpreise[kartenTyp-1];
		
	    System.out.print("Anzahl der Tickets (1-10):");
	    double anzahlTicket = tastatur.nextInt();
	    if (anzahlTicket <= 10 && anzahlTicket >= 1) 
	    {
	    	zuZahlenderBetrag *= anzahlTicket;
	    	return zuZahlenderBetrag; 
	    }
	    else { 
	    	System.out.println("Leider wurde eine falsche Anzahl eingegeben, es wird mit 1 Ticket weitergearbeitet.");
	    	anzahlTicket = 1;
	    	zuZahlenderBetrag *= anzahlTicket;
	    	return zuZahlenderBetrag;
	    }
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);
		
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.println("Noch zu zahlen: " + String.format("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag)) + " Euro"); // Addition
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   double eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze; 
	       }
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
		System.out.println("Fahrschein wird ausgegeben.");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n");
	       System.out.println("\nVergessen Sie nicht, den Fahrschein entwerten zu lassen!\n"
	    		   		+ "Wir wünschen Ihnen eine gute Fahrt.\n");
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) {
		double rückgabebetrag;
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;//Subtraktion
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("\nDer Rückgabebetrag in Höhe von " + String.format("%.2f", rückgabebetrag) + " EURO"); //Addition
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen 
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	}
}