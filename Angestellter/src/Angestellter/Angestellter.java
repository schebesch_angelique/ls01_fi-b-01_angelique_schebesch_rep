package Angestellter;

public class Angestellter {

	 private String name;
	 private String vorname;
	 private double gehalt;

	 public Angestellter() {
		 name = "Mustermann";
		 vorname = "Max";
		 gehalt = 1_000d;
	 }
	 
	 public Angestellter(String vorname, String name) {
		 this();
		 this.vorname = vorname;
		 this.name = name;
	 }
	 
	 public Angestellter(String vorname, String name, double gehalt) {
		 this(vorname, name);
		 this.vorname = vorname;
		 this.name = name;
		 this.gehalt = gehalt; 
	 }
	 
	 public void setName(String name) {
	    this.name = name;
	 }
	 
	 public String getName() {
	    return this.name;
	 }

	 public void setGehalt(double gehalt) {
		 if(gehalt < 0)
			 return;
		 this.gehalt = gehalt;
	 }
 
	 public double getGehalt() {
		 return gehalt;
	 }

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	 
	public String vollname() {
		String vollname = getVorname() +" "+ getName();
		return vollname;
	}
}
