package Angestellter;

public class TestAngestellter {

	public static void main(String[] args) {
		  
		//Erzeugen der Objekte
		Angestellter ang1 = new Angestellter ( "Manfred", "Meier", 4500 );
	    Angestellter ang2 = new Angestellter ( "Petersen", "Peter", 6000 ); 
	    Angestellter ang3 = new Angestellter ( "Garfield", "W�rze", 5000 );
	    Angestellter ang4 = new Angestellter ( "Mia", "Hensen");
	    Angestellter ang5 = new Angestellter ( "Oliver", "Mango");
	    

	    ang4.setGehalt(2500);
	    ang5.setGehalt(6399);
	    
        //Bildschirmausgabe
	    System.out.println("Name: " + ang1.getName());
	    System.out.println("Vorname: " + ang1.getVorname());
	    System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
	    System.out.println("Vollname: " + ang1.vollname());
        System.out.println("\nName: " + ang2.getName());
	    System.out.println("Vorname: " + ang2.getVorname());
	    System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
	    System.out.println("Vollname: " + ang2.vollname());
	    System.out.println("\nName: " + ang3.getName());
	    System.out.println("Vorname: " + ang3.getVorname());
	    System.out.println("Gehalt: " + ang3.getGehalt() + " Euro");
	    System.out.println("Vollname: " + ang3.vollname());
	    System.out.println("\nName: " + ang4.getName());
	    System.out.println("Vorname: " + ang4.getVorname());
	    System.out.println("Gehalt: " + ang4.getGehalt() + " Euro");
	    System.out.println("Vollname: " + ang4.vollname());
	    System.out.println("\nName: " + ang5.getName());
	    System.out.println("Vorname: " + ang5.getVorname());
	    System.out.println("Gehalt: " + ang5.getGehalt() + " Euro");
	    System.out.println("Vollname: " + ang5.vollname());
	}

}
