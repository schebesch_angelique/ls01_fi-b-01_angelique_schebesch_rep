import java.util.Scanner;

public class schaltjahr {

	public static void main(String[] args) {
		
		int jahreszahl = eingabe();		
		ausgabe(jahreszahl);
	}
	
	public static int eingabe() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Dieser Programm verr�t Ihnen, ob Ihre eingegebene Jahreszahl ein Schaltjahr ist.");
		System.out.println("Bitte geben Sie nun die vierstellige Jahreszahl ein.(Mindestjahr ist: 0001)");
		int jahreszahl = sc.nextInt();
		return jahreszahl;
	} 
	
	public static void ausgabe(int jahreszahl) {
		if(jahreszahl %4 == 0) {
    		if(jahreszahl %100 == 0) {
    			if (jahreszahl %400 == 0 && jahreszahl >= 1582) {
    				System.out.println("Die Jahreszahl " + jahreszahl + " ist ein Schaltjahr.");
    			} 
    			else {
    				System.out.println("Die Jahreszahl " + jahreszahl + " ist kein Schaltjahr.");
    			}
    		} 
    		else {
    			System.out.println("Die Jahreszahl " + jahreszahl + " ist ein Schaltjahr.");
    		}
    	} 
    	else {
    		 System.out.println("Die Jahreszahl " + jahreszahl + " ist kein Schaltjahr.");
    	}
	}
}