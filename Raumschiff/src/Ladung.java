/**
 * Dies ist die Klasse Ladung, hiervon kann ein Objekt erzeugt werden um anschlie�end einem Raumschiff hinzuzuf�gen
 * @author Ang�lique Schebesch
 * @version 1.0 vom 12.04.2021
 */
public class Ladung {

	private String bezeichnung;
	private int menge;
	
	/**
	 * Dies ist der Standardkonstruktor f�r die Klasse Ladung.
	 */
	public Ladung() {
		this.bezeichnung = "";
		this.menge = 0;
	}
	/**
	 * Dies ist der zweite Konstruktur, wenn die Bezeichnung und Anzahl der Ladung von Anfang an bekannt ist.
	 * @param bezeichnung
	 * @param anzahl
	 */
	public Ladung(String bezeichnung, int anzahl) {
		this();
		if(bezeichnung == null && anzahl < 0)
			return;
		this.bezeichnung = bezeichnung;
		this.menge = anzahl;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int anzahl) {
		this.menge = anzahl;
	}
}
