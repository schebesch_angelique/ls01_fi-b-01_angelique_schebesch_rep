import java.util.*;

/**
 * Dies ist die Klasse der Raumschiffe in denen die folgenden Attribute und Methoden aufgef�hrt werden.
 * @author Ang�lique Schebesch
 * @version 1.0 vom 12.04.2021
 */
public class Raumschiff {

	private String schiffsname;
	private int energieversorgungInProzent;
	private int schutzschildInProzent;
	private int lebenserhaltungssystemInProzent;
	private int huelleInProzent;
	private int androidenAnzahl;
	private int photonentorpedoAnzahl;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	Random r = new Random();

	/**
	 * Dies ist der Standardkonstruktor f�r die Klasse Raumschiff.
	 */
	public Raumschiff() {
		schiffsname = "Raumschiff 1";
		energieversorgungInProzent = 100;
		schutzschildInProzent = 100;
		lebenserhaltungssystemInProzent = 100;
		huelleInProzent = 100;
		androidenAnzahl = 2;
		photonentorpedoAnzahl = 2;
		ladungsverzeichnis = new ArrayList<>();
		broadcastKommunikator = new ArrayList<>();
	}

	/**
	 * Dies ist der zweite Konstruktor, wenn der Name, die Prozentwerte von Energieversorgung, Schutzschild, Lebenserhaltungssystem, H�lle, 
	 * sowie die Anzahl der ReperaturAndroiden & Photonentorpedos bekannt ist. 
	 * @param name
	 * @param energieversorgung
	 * @param schutzschild
	 * @param lebenserhaltungssystem
	 * @param huelle
	 * @param reperaturAndroiden
	 * @param photonentorpedos
	 */
	public Raumschiff(String name, int energieversorgung, int schutzschild, int lebenserhaltungssystem, int huelle,
			int reperaturAndroiden, int photonentorpedos) {
		this();
		this.schiffsname = name;
		this.schutzschildInProzent = schutzschild;
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystem;
		this.huelleInProzent = huelle;
		this.androidenAnzahl = reperaturAndroiden;
		this.photonentorpedoAnzahl = photonentorpedos;
	}

	// Methoden

	/**
	 * Diese Methode f�gt dem Ladeverzeichnis ein Objekt von der Klasse Ladung hinzu.
	 * @param ladung
	 */
	public void addLadung(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}

	/**
	 * Mit dieser Methode wird lediglich der Zustand des Raumschiffes ausgegeben, sodass man die aktuellen Werte sehen kann.
	 */
	public void zustand() {
		System.out.println("\nDer Zustand des Raumschiffes: " + schiffsname + "\nEnergieversorgung: "
				+ energieversorgungInProzent + "\nSchutzschild: " + schutzschildInProzent + "\nLebenserhaltungssystem: "
				+ lebenserhaltungssystemInProzent + "\nH�lle: " + huelleInProzent + "\nReperatur-Androiden: "
				+ androidenAnzahl + "\nPhotonentorpedos: " + photonentorpedoAnzahl);
	}

	/**
	 * Diese Mehtode gibt nacheinander den Inhalt des Ladungsverzeichnisses aus, sodass man die Bezeichnung und Ladungsmenge erh�lt.
	 */
	public void ladungAnzeigen() {
		System.out.println("\nDas Lager beinhaltet: ");
		for (Ladung i : ladungsverzeichnis) {
			System.out.println(i.getBezeichnung() + " x " + i.getMenge());
		}
	}

	/**
	 * Hier wird in der Methode das Raumschiff �bergeben, welches die Funktion ausf�hren soll. Es sollen Photonentorpedos abgeschossen werden
	 * und anschlie�end eine Nachricht an alle �bergeben werden. Dabei wird die Methode 'nachrichtAnAlle' genutzt. Sind keine Photonentorpedos vorhanden
	 * so wird die Nachricht '-=*Click*=-' ausgegeben, sind jedoch welche vorhanden wird 'Photonentorpedo wurde abgeschossen' ausgegeben, anschlie�end wird 
	 * die Methode 'treffer' bei erfolgreichem Abschuss ausgef�hrt.
	 * @param r Ein Objekt der Klasse Raumschiff, welches getroffen werden soll
	 */
	public void abschie�enPhotonentorpedos(Raumschiff r) {
		if (photonentorpedoAnzahl <= 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			nachrichtAnAlle("Photonentorpedo wurde abgeschossen");
			this.photonentorpedoAnzahl--;
			treffer(r);
		}
	}

	/**
	 * Hier wird in der Methode das Raumschiff �bergeben, welches die Funktion ausf�hren soll. Es sollen Phaserkanonen abgeschossen werden
	 * und anschlie�end eine Nachricht an alle �bergeben werden. Dabei wird die Methode 'nachrichtAnAlle' genutzt. Sind keine Phaserkanonen vorhanden
	 * so wird die Nachricht '-=*Click*=-' ausgegeben, sind jedoch welche vorhanden wird 'Phaserkanonen wurde abgeschossen' ausgegeben, anschlie�end wird 
	 * die Methode 'treffer' bei erfolgreichem Abschuss ausgef�hrt.
	 * @param r Ein Objekt der Klasse Raumschiff, welches getroffen werden soll
	 */
	public void abschie�enPhaserkanonen(Raumschiff r) {
		if (energieversorgungInProzent <= 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			nachrichtAnAlle("Phaserkanone wurde abgeschossen");
			this.energieversorgungInProzent -= 50;
			treffer(r);
		}
	}

	/**
	 * Die Methode 'treffer' umfasst den Schaden der bei einem Raumschiff angerichtet wird. Hierbei wird in der Konsole die Nachricht 'Raumschiff wurde getroffen!'
	 * ausgegeben. Ebenfalls wird zuerst dem Schutzschild 50 Prozent abgezogen werden. Ist diese bei Null wird dann der H�lle was abgezogen.
	 * Ist diese auch bei Null wird das Lebenserhaltungssystem auf Null gesetzt. Anschlie�end wird die Nachricht 'Alle Lebenserhaltungssysteme wurden vernichtetet'
	 * ausgegeben.
	 * @param r Ein Objekt der Klasse Raumschiff, welches getroffen wird
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + "wurde getroffen!");
		r.setSchutzschildInProzent(r.getSchutzschildInProzent()-50);
		if (r.getSchutzschildInProzent() == 0) {
			r.setHuelleInProzent(getHuelleInProzent()-50);
			if (r.getHuelleInProzent() == 0) {
				r.setLebenserhaltungssystemInProzent(0);
				nachrichtAnAlle("Alle Lebenserhaltungssysteme wurden vernichtetet");
			}
		}
	}

	/**
	 * Die Methode 'nachrichtAnAlle' f�gt die �bergebene Nachricht dem BroadcastKommunikator hinzu und gibt sie au�erdem in der Konsole auch aus.
	 * @param nachricht Ein String, der �bermittelt wird
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
	}

	/**
	 * Diese Methode gibt den broadcastKommunikator lediglich zur�ck.
	 * @return broadcastKommunikator
	 */
	public ArrayList<String> eintraegeLogbuch() {
		return broadcastKommunikator;
	}

	/**
	 * In dieser Methode werden die gew�nschte Torpedo Anzahl zum Laden �bergeben. Anschlie�end wird in der Liste Ladungsverzeichnis
	 * nach Photonentorpedos gesucht um diese anschlie�end zu verwenden. Wenn welche gefunden worden sind, wird entweder die gew�nschte 
	 * Anzahl oder die maximale Vorhandenden Photonentorpedos geladen. Ebenfalls wird auf der Konsole die Nachricht 
	 * 'Anzahl + Photonentorpedo(s) eingesetzt' ausgegeben. Sollten jedoch keine Photonetorpedos gefunden
	 * worden sein, so wie in der Konsole 'Keine Photonentorpedos gefunden!' ausgegeben. 
	 * @param anzahlTorpedos Gew�nschte Anzahl der Torpedos, die geladen werden sollen
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		Ladung p = null;
		for (Ladung l : ladungsverzeichnis) {
			if (l.getBezeichnung().equals("Photonentorpedo")) {
				p = l;
			}
		}
		if (p != null) {
			if (p.getMenge() > anzahlTorpedos) {
				setPhotonentorpedoAnzahl(p.getMenge());
				System.out.println(p.getMenge() + " Photonentorpedo(s) eingesetzt.");
			} else {
				p.setMenge(p.getMenge() - anzahlTorpedos);
				setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + anzahlTorpedos);
				System.out.println(anzahlTorpedos + " Photonentorpedo(s) eingesetzt.");
			}
		} else {
			System.out.println("\nKeine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * In dieser Methode wird das Ladungsverzeichnis aufger�umt, indem es nach Objekten mit der Ladungsmenge Null sucht um diese anschlie�end
	 * aus dem Ladungsverzeichnis zu entfernen.
	 */
	public void ladungAufraeumen() {
		ArrayList<Ladung> newLadungsverzeichnis = new ArrayList<>();
		for (Ladung l : ladungsverzeichnis) {
			if (l.getMenge() != 0) {
				newLadungsverzeichnis.add(l);
			}
		}
		ladungsverzeichnis = newLadungsverzeichnis;
	}

	/**
	 * In dieser Methode  wird per Zufallszahl, der Anzahl auf true gesetzten Schiffstrukturen und Anzahl der Droiden eine Gleichung
	 * aufgestellt, in der der Wert der Reperatur berechnet wird. Anschlie�end erh�lt jede true gesetzte Schiffstruktur diesen Wert
	 * auf ihren bisherigen Wert der Schiffsstruktur darauf. Zuletzt wird die Nachricht 'Es wurde repariert.' in der Konsole ausgegeben.
	 * @param schutzschilde Boolean, der entweder auf true oder false gesetzt wird
	 * @param energieversorgung Boolean, der entweder auf true oder false gesetzt wird
	 * @param schiffshuelle Boolean, der entweder auf true oder false gesetzt wird
	 * @param anzahlDroiden Anzahl der gew�nschten Droiden
	 */
	public void reperaturAndroidenEinsetzen(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
		int zufallszahl = r.nextInt(99)+ 1;
		int anzahlD;
		int anzahlS = 0;

		if (schutzschilde == true)
			anzahlS++;
		if (energieversorgung == true)
			anzahlS++;
		if (schiffshuelle == true)
			anzahlS++;

		if (anzahlDroiden > androidenAnzahl) {
			anzahlD = androidenAnzahl;
		} else {
			anzahlD = anzahlDroiden;
		}
		int reparatur = (zufallszahl * anzahlD) / anzahlS;

		if (schutzschilde == true)
			setSchutzschildInProzent(getSchutzschildInProzent() + reparatur);
		if (energieversorgung == true)
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + reparatur);
		if (schiffshuelle == true)
			setHuelleInProzent(getHuelleInProzent() + reparatur);
		System.out.println("\nEs wurde repariert.");
	}

	// getter & setter
	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * In dem Setter wird gepr�ft ob der �bergebene Wert �ber Einhundert und �ber Null ist, da der Wert des Attributs nicht �ber
	 * Einhundert steigen oder unter Null sinken darf.
	 * @param energieversorgungInProzent Aber ohne Prozentzeichen
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		if (energieversorgungInProzent > 100) {
			this.energieversorgungInProzent = 100;
		} else if (energieversorgungInProzent > 0) {
			this.energieversorgungInProzent = energieversorgungInProzent; 
			}
	}

	public int getSchutzschildInProzent() {
		return schutzschildInProzent;
	}

	/**
	 * In dem Setter wird gepr�ft ob der �bergebene Wert �ber Einhundert und �ber Null ist, da der Wert des Attributs nicht �ber
	 * Einhundert steigen oder unter Null sinken darf.
	 * @param schutzschildInProzent Aber ohne Prozentzeichen
	 */
	public void setSchutzschildInProzent(int schutzschildInProzent) {
		if (schutzschildInProzent > 100) {
			this.schutzschildInProzent = 100;
		} else if (schutzschildInProzent > 0) {
			this.schutzschildInProzent = schutzschildInProzent;
		}
	}

	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}

	/**
	 * In dem Setter wird gepr�ft ob der �bergebene Wert �ber Einhundert und �ber Null ist, da der Wert des Attributs nicht �ber
	 * Einhundert steigen oder unter Null sinken darf.
	 * @param lebenserhaltungssystemInProzent Aber ohne Prozentzeichen
	 */
	public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
		if (lebenserhaltungssystemInProzent > 100) {
			this.lebenserhaltungssystemInProzent = 100;
		} else if (lebenserhaltungssystemInProzent > 0) {
			this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
		}
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * In dem Setter wird gepr�ft ob der �bergebene Wert �ber Einhundert und �ber Null ist, da der Wert des Attributs nicht �ber
	 * Einhundert steigen oder unter Null sinken darf.
	 * @param huelleInProzent Aber ohne Prozentzeichen
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		if (huelleInProzent > 100) {
			this.huelleInProzent = 100;
		} else if (huelleInProzent > 0) {
			this.huelleInProzent = huelleInProzent;
		}
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
}
