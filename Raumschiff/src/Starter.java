/**
 * Dies ist die Starter Klasse f�r die Raumschiffe, in der sich die Main Funktion befindet
 * @author Ang�lique Schebesch
 * @version 1.0 vom 12.04.2021
 *
 */

public class Starter {

/** 
 * Es folgt die Main Funktion, welche als Starter f�r die 'Quellcode verwenden'Aufgabe dient. Diese sind:
 * 1. Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.
 * 2. Die Romulaner schie�en mit der Phaserkanone zur�ck.
 * 3. Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.
 * 4. Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
 * 5. Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein (f�r Experten).
 * 6. Die Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren Ihres Raumschiffes und r�umen das Ladungsverzeichnis auf (f�r Experten).
 * 7. Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.
 * 8. Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
 * @param args Kommandozeileparameter
 */
	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 2, 1);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 100, 100, 100, 100, 5, 0);
		
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 20);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);
		
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);
		
		
		klingonen.abschie�enPhotonentorpedos(romulaner);
		romulaner.abschie�enPhaserkanonen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustand();
		klingonen.ladungAnzeigen();
		vulkanier.reperaturAndroidenEinsetzen(true, true, true, vulkanier.getAndroidenAnzahl());
		vulkanier.photonentorpedosLaden(l7.getMenge());
		vulkanier.ladungAufraeumen();
		klingonen.abschie�enPhaserkanonen(romulaner);
		klingonen.zustand();
		klingonen.ladungAnzeigen();
		romulaner.zustand();
		romulaner.ladungAnzeigen();
		vulkanier.zustand();
		vulkanier.ladungAnzeigen();
		
		
	}

}
