import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		int min = 0;
		int max = 99;
		double summe = 0;
		double mittelwert;
		
		System.out.println("Wie viele Zahlen sollen per Zufall zwischen 0 und 99 erzeugt werden?");
		int ausgabe = tastatur.nextInt();
		int [] zahlen = new int[ausgabe];
		
		for (int i = 0; i < ausgabe; i++) {
			zahlen[i] = (int) Math.random() * (max- min - 1) + min; 
			summe += zahlen[i];
		}
		
		System.out.println("Dies sind die zufälligen Zahlen");
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] + " ");
		}
		
		mittelwert = summe / ausgabe;
		System.out.println("Dies ist der Mittelwert:" + mittelwert);
	}
}
	